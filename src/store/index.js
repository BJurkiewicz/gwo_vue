import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    books: []
  },
  mutations: {
    SET_BOOKS (state, data) {
      state.books = data
    }
  },
  actions: {
    getBooks ({commit}, query) {
      return axios.get('https://gwo.pl/booksApi/v1/search?query=' + query)
      .then(res => {
        commit('SET_BOOKS', res.data)
      })
      .catch(err => {
        console.error(err)
      })
    }
  }
})
